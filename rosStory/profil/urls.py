from django.urls import path
from . import views

urlpatterns = [
    path('', views.cover, name='cover'),
    path('/story4.html', views.profil, name='profil'),
]